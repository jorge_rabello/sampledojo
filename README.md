Dada uma Carteira Virtual, esta deve ter as seguintes funcionalidades:

    - [x] Consultar moedas

      [GET] http://localhost:8080/currencies

    - [x] Criar novas moedas

      [POST] http://localhost:8080/currencies
      Body:
        ```
          {
              "alias":"BRL"
              "name":"Real"
          }
        ```

    - [ ] Consultar taxas de câmbio de determinada moeda

    - [ ] Realizar transações de câmbio entre moedas, por exemplo, converter dolar em reais e vice versa


Para as 2 tarefas, utilizar o arquivo currencies_rate.json, nele contém as informações necessárias para a criação dos 2 endpoints.

currencies_rate.json:

```
[{
	"base": "USD",
	"rates": [{
			"alias": "BRL",
			"value": 3.68,
			"name": "Real"
		},
		{
			"alias": "USD",
			"value": 1,
			"name": "Doleta"
		},
		{
			"alias": "EUR",
			"value": 1.1522,
			"name": "Euro"
		}
	]
}]
```