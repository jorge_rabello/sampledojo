package rate

import exceptions.EmptyRateException
import exceptions.NotFoundRateException

class RateServiceImpl : RateService {
    override fun findRate(rate: Rate): Double? {

        if (rate.currency_input.isNullOrEmpty()) {
            throw EmptyRateException("A moeda de entrada não está preenchida")
        } else if (rate.currency_output.isNullOrEmpty()) {
            throw EmptyRateException("A moeda de saída não está preenchida")
        }


        val currencyDto = CurrencyRepository.currencies.filter {
            it.base == rate.currency_input
        }.firstOrNull()?.rates?.filter {
            it.alias == rate.currency_output
        }?.firstOrNull() ?: throw NotFoundRateException("Moeda de Entrada ou Saída Não Foi Encontrada!")

        return currencyDto?.value
    }

}