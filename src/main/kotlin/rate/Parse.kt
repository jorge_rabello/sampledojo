package rate

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.type.TypeReference
import java.io.File
import java.nio.file.Paths
import java.util.*

object CurrencyRepository {

    var currencies: List<CurrencyDto>

    init {
        this.currencies = load()
    }

    fun load(): List<CurrencyDto> {
        val filePath =
            Paths.get(CurrencyDto::class.java.classLoader.getResource("currencies_rate.json").toURI()).toAbsolutePath()
                .toString()
        return ObjectMapper().readValue(File(filePath), object : TypeReference<List<CurrencyDto>>() {})
    }

}

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    value = [
        "base",
        "rates"
    ]
)
class CurrencyDto {

    @JsonProperty("base")
    @get:JsonProperty("base")
    @set:JsonProperty("base")
    var base: String? = null
    @JsonProperty("rates")
    @get:JsonProperty("rates")
    @set:JsonProperty("rates")
    var rates: List<RateDto>? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("alias", "value", "name")
class RateDto {

    @JsonProperty("alias")
    @get:JsonProperty("alias")
    @set:JsonProperty("alias")
    var alias: String? = null
    @JsonProperty("value")
    @get:JsonProperty("value")
    @set:JsonProperty("value")
    var value: Double? = null
    @JsonProperty("name")
    @get:JsonProperty("name")
    @set:JsonProperty("name")
    var name: String? = null
    @JsonIgnore
    private val additionalProperties = HashMap<String, Any>()

    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties[name] = value
    }

}