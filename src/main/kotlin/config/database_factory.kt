package config

import org.jetbrains.exposed.sql.Database


object DatabaseFactory {



    fun init(
        databaseURL: String,
        databaseDriver: String,
        databaseUsername: String,
        datbasePassword: String
    ) {

        Database.connect(
            url = databaseURL,
            driver = databaseDriver,
            user = databaseUsername,
            password = datbasePassword
        )
    }

}