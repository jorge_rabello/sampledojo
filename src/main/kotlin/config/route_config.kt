package config

import currency.CurrencyController
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder
import rate.RateController

class RouteConfig(private val currencyController: CurrencyController, private val rateController: RateController) {

    fun register(app: Javalin) {

        app.get("/") { ctx -> ctx.json(mapOf("message" to " it's adventure time !")) }

        app.routes {
            ApiBuilder.path("currencies") {
                ApiBuilder.get(currencyController::findAll)
                ApiBuilder.post(currencyController::create)
            }
            ApiBuilder.path("rates") {
                ApiBuilder.get(rateController::findRate)
            }
        }
    }
}