package config

import currency.CurrencyController
import currency.CurrencyDao
import currency.CurrencyService
import currency.CurrencyServiceImpl
import org.koin.dsl.module.module
import rate.RateController
import rate.RateService
import rate.RateServiceImpl

object KoinModuleConfig {
    val applicationModule = module {
        single { CurrencyDao() }
        single { CurrencyServiceImpl(get()) as CurrencyService }
        single { RateServiceImpl() as RateService }
        single { RouteConfig(get(), get()) }
        single { CurrencyController(get()) }
        single { RateController(get()) }
    }
}