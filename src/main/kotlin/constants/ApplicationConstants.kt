package constants

object ApplicationConstants {

    // application server
    const val SERVER_PORT = "server_port"
    const val DEFAULT_SERVER_PORT = 7000

    // database
    const val DATABASE_URL = "database_url"
    const val DATABASE_DRIVER = "database_driver"
    const val DATABASE_USERNAME = "database_username"
    const val DATABASE_NAME = "database_name"
    const val DATABASE_PASSWORD = "database_password"
}