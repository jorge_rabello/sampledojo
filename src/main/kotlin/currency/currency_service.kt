package currency

interface CurrencyService {
    fun findAll() : List<Currency>
    fun create(currency: Currency): Currency
}