package currency

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class CurrencyDao {

    object Currencies : Table("currency") {
        val alias = varchar("alias", 3)
        val name = varchar("name", 20)
    }

    fun mapCurrency (it: ResultRow) : Currency =
        Currency(
            it[Currencies.alias],
            it[Currencies.name]
        )


    fun findAll () = transaction {
        Currencies.selectAll().map { mapCurrency(it)}
    }

    fun create(currency: Currency): Currency = transaction {
        Currencies.insert {
            it[Currencies.alias] = currency.alias
            it[Currencies.name] = currency.name
        }

        currency
    }

    fun getCurrency(alias: String): Currency? = transaction {
        Currencies.select({Currencies.alias eq alias}).map { mapCurrency(it)}.firstOrNull()
    }

}