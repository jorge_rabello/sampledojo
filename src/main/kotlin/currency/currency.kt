package currency


data class Currency(val alias: String, val name: String)
