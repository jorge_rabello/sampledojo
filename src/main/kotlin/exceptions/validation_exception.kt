package exceptions

import java.lang.Exception

class ValidationException(msg: String) : Exception(msg)