package exceptions

class CreateCurrencyException(msg: String) : Exception(msg)