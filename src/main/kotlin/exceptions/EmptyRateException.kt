package exceptions

class EmptyRateException(msg: String) : Exception(msg)
